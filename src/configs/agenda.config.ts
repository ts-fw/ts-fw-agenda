import {Config, IConfigStorage} from 'ts-fw';
import {inject, Container} from 'inversify';
import * as winston from 'winston';
import * as Agenda from 'agenda';
import {AgendaConfigModel} from '../models/configs';
import {MongoClient} from 'mongodb';
import * as process from 'process';

@Config(2)
export class AgendaConfigurator {
    private agenda: Agenda;

    private readonly jobs: string[] = [];

    @inject('configStorage')
    private configStorage: IConfigStorage;

    @inject('logger')
    private logger: winston.Winston;

    @inject('di')
    private container: Container;

    async configuration(): Promise<void> {
        const agendaConfig = this.configStorage.get<AgendaConfigModel>('agenda');

        if (!agendaConfig) {
            this.logger.info('Cannot configure connection to Agenda');
            return;
        }

        process.on('exit', this._onExitHandler.bind(this, {cleanup: true}));
        process.on('SIGINT', this._onExitHandler.bind(this, {exit: true}));
        process.on('uncaughtException', this._onExitHandler.bind(this, {exit: true}));

        await this._configuring(agendaConfig);
    }

    private async _configuring(config: AgendaConfigModel): Promise<void> {
        try {
            const mongoConnectionURI = config.connection;
            const mongoDriver = await MongoClient.connect(mongoConnectionURI);

            this.agenda = new Agenda().mongo(mongoDriver, config.collection || 'jobs');

            await new Promise((resolve: Function) => this.agenda.once('ready', resolve()));

            this.container.bind('agendaService').toConstantValue(this.agenda);

            const jobs = this.container.getAll<Function[]>('Job');

            if (jobs.length === 0) {
                this.logger.error('Agenda jobs not found. Use @Job decorator for ');
                return;
            }

            for (const job of jobs) {
                const name: string = Reflect.getMetadata('name', job);
                const interval: string = Reflect.getMetadata('interval', job);
                const method: string = Reflect.getMetadata('method', job);

                this.agenda.define(name, async (jobArg: Agenda.Job, done: Function) => {
                    const result = job[method](jobArg);

                    if (Promise.resolve(result) === result) {
                        try {
                            await result;
                        } catch (error) {
                            this.logger.error(error);
                        }
                    }

                    done();
                });

                if (interval) {
                    this.agenda.every(interval, name);
                }

                this.jobs.push(name);
            }

            this.agenda.start();
        } catch (error) {
            this.logger.error(error);
        }
    }

    private async _onExitHandler(options, err) {
        if (err) {
            console.log(err.stack);
        }

        if (options.exit) {
            for (const job of this.jobs) {
                await this.agenda.cancel({
                    name: job
                });
            }
            this.agenda.stop(() => {
                process.exit();
            });
        }
    }
}
