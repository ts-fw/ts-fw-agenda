import {Injectable, ReflectUtils} from 'ts-fw';

function formatName(str: string, divider: string = ':') {
    return str.replace(/(?:^\w|[A-Z]|\b\w|\s+)/g, (match, index) => {
        if (+match === 0) return '';
        return index == 0 ? match.toLowerCase() : `${divider}${match.toLowerCase()}`;
    });
}

export interface JobOptions {
    name?: string;
    interval?: string;
}

export function Job(options?: JobOptions): ClassDecorator {
    return (constructor: Function) => {
        Reflect.decorate([Injectable('Job')], constructor);
        let jobName: string;
        if (options && options.name) {
            jobName = options.name;
            if (options.name.split(':').length === 0) {
                jobName = formatName(options.name);
            }
        } else {
            jobName = formatName(constructor.name);
        }

        Reflect.defineMetadata('name', jobName, constructor.prototype);
        if (options && options.interval) {
            Reflect.defineMetadata('interval', options.interval, constructor.prototype);
        }

        ReflectUtils.annotateMethod(constructor);
    }
}
